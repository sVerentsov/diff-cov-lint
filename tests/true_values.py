from pathlib import Path

true_processed_diff = {
    'src/add.py': [1, 2, 3, 5, 6, 7, 9, 10, 11],
    'src/modify.py': [2, 6],
    'test/test_all.py': [6, 8, 11, 12, 15]
}

true_processed_cov = {
    'src/__init__.py': {},
    'src/add.py': {
        1: 1,
        2: 0,  # function_add_not_covered
        3: 0,
        5: 1,
        6: 1,
        7: 1,
        9: 1,
        10: 0,  # function_add_badcode
        11: 0
    },
    'src/modify.py': {
        1: 1,
        2: 0,  # function_modify_not_covered
        3: 0,
        5: 1,
        6: 1,
        7: 1,
        9: 1,
        10: 0  # function_modify_badcode
    },
    'src/rename_after.py': {
        1: 1,
        2: 0,  # function_rename_not_covered
        3: 0,
        5: 1,
        6: 1,
        7: 1,
        9: 1,
        10: 0  # function_rename_badcode
    }
}

true_filtered_cov = {
    'src/add.py': {
        1: 1,
        2: 0,
        3: 0,
        5: 1,
        6: 1,
        7: 1,
        9: 1,
        10: 0,
        11: 0
    },
    'src/modify.py': {
        2: 0,
        6: 1
    }
}

true_report = """======================== DIFF COVERAGE ========================
FILE                                    COVERED STMTS   PERCENT
src/add.py                                    5     9     55.6%
src/modify.py                                 1     2     50.0%
===============================================================
TOTAL DIFF COV                                6    11     54.5%
"""

true_report_missing = """======================== DIFF COVERAGE ========================
FILE                                    COVERED STMTS   PERCENT MISSING
src/add.py                                    5     9     55.6% 2-3, 10-11
src/modify.py                                 1     2     50.0% 2
===============================================================
TOTAL DIFF COV                                6    11     54.5%
"""

true_processed_lint = {
    'src/add.py': {
        10: {
            'error':
            'E0602',
            'msg':
            "Undefined variable 'this_line_makes_no_sense' (undefined-variable)"
        },
        11: {
            'error':
            'E0602',
            'msg':
            "Undefined variable 'this_line_also_makes_no_sense' (undefined-variable)"
        }
    },
    'src/modify.py': {
        10: {
            'error':
            'E0602',
            'msg':
            "Undefined variable 'this_line_makes_no_sense' (undefined-variable)"
        }
    },
    'src/rename_after.py': {
        10: {
            'error':
            'E0602',
            'msg':
            "Undefined variable 'this_line_makes_no_sense' (undefined-variable)"
        }
    }
}

true_filtered_lint = {
    'src/add.py': {
        10: {
            'error':
            'E0602',
            'msg':
            "Undefined variable 'this_line_makes_no_sense' (undefined-variable)"
        },
        11: {
            'error':
            'E0602',
            'msg':
            "Undefined variable 'this_line_also_makes_no_sense' (undefined-variable)"
        }
    }
}

true_lint_report = """========================== DIFF LINT ==========================
src/add.py:10:0 E0602: Undefined variable 'this_line_makes_no_sense' (undefined-variable)
src/add.py:11:0 E0602: Undefined variable 'this_line_also_makes_no_sense' (undefined-variable)

"""

true_full_report = """======================== DIFF COVERAGE ========================
FILE                                    COVERED STMTS   PERCENT
src/add.py                                    5     9     55.6%
src/modify.py                                 1     2     50.0%
===============================================================
TOTAL DIFF COV                                6    11     54.5%

========================== DIFF LINT ==========================
src/add.py:10:0 E0602: Undefined variable 'this_line_makes_no_sense' (undefined-variable)
src/add.py:11:0 E0602: Undefined variable 'this_line_also_makes_no_sense' (undefined-variable)

"""

diff --git a/src/add.py b/src/add.py
new file mode 100644
index 0000000..972f2ed
--- /dev/null
+++ b/src/add.py
@@ -0,0 +1,10 @@
+def function_add_not_covered():
+    a = 2 + 3
+    return a
+
+def function_add_covered():
+    a = 2 + 3
+    return a
+
+def function_add_badcode():
+    this_line_makes_no_sense()
diff --git a/src/delete.py b/src/delete.py
deleted file mode 100644
index 2c93e20..0000000
--- a/src/delete.py
+++ /dev/null
@@ -1,10 +0,0 @@
-def function_delete_not_covered():
-    a = 2 + 3
-    return a
-
-def function_delete_covered():
-    a = 2 + 3
-    return a
-
-def function_delete_badcode():
-    this_line_makes_no_sense()
diff --git a/src/modify.py b/src/modify.py
index 59b44e7..ee4e370 100644
--- a/src/modify.py
+++ b/src/modify.py
@@ -1,9 +1,9 @@
 def function_modify_not_covered():
-    a = 2 + 3
+    a = 2 + 4
     return a
 
 def function_modify_covered():
-    a = 2 + 3
+    a = 2 + 4
     return a
 
 def function_modify_badcode():
diff --git a/src/rename_before.py b/src/rename_after.py
similarity index 100%
rename from src/rename_before.py
rename to src/rename_after.py
diff --git a/test/test_all.py b/test/test_all.py
index 520dddb..a954e36 100644
--- a/test/test_all.py
+++ b/test/test_all.py
@@ -3,16 +3,16 @@ from pathlib import Path
 import unittest
 sys.path.append(str(Path(__file__).parent.parent))
 
-from src.delete import function_delete_covered
+from src.add import function_add_covered
 from src.modify import function_modify_covered
-from src.rename_before import function_rename_covered
+from src.rename_after import function_rename_covered
 
 class Tests(unittest.TestCase):
-    def test_delete(self):
-        self.assertEqual(function_delete_covered(), 5)
+    def test_add(self):
+        self.assertEqual(function_add_covered(), 5)
     
     def test_modify(self):
-        self.assertEqual(function_modify_covered(), 5)
+        self.assertEqual(function_modify_covered(), 6)
 
     def test_rename(self):
         self.assertEqual(function_rename_covered(), 5)
\ No newline at end of file
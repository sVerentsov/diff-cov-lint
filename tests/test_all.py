import os
import re
import subprocess
import sys
import unittest
from pathlib import Path

import pytest

sys.path.insert(0, str(Path(__file__).parent.parent))

from diff_cov_lint.coverage import (diff_coverage_report, filter_coverage,
                                    process_coverage_xml, produce_ranges)
from diff_cov_lint.diff import get_diff, process_diff
from diff_cov_lint.lint import (diff_lint_report, filter_lint,
                                process_pylint_report)
from diff_cov_lint.main import diff_cov_lint
from true_values import (true_filtered_cov, true_filtered_lint,
                         true_full_report, true_lint_report,
                         true_processed_cov, true_processed_diff,
                         true_processed_lint, true_report, true_report_missing)

REPO_PATH = (Path(__file__).parent / "repo").resolve()
COVERAGE_XML_PATH = REPO_PATH / "coverage.xml"
PYLINT_OUTPUT_PATH = REPO_PATH / "pylint_output.txt"
TRUE_DIFF_PATH = REPO_PATH / "TRUE_DIFF.txt"


class DiffCovTest(unittest.TestCase):
    def assertStrAlmostEqual(self, first: str, second: str):
        self.assertEqual(
            re.sub(r"(\s)+", " ", first).strip(),
            re.sub(r"(\s)+", " ", second).strip())

    # This fixture is for print testing
    @pytest.fixture(autouse=True)
    def _pass_fixtures(self, capsys):
        self.capsys = capsys

    @classmethod
    def setUpClass(cls):
        global TRUE_DIFF
        first_pwd = os.getcwd()
        os.chdir(REPO_PATH)
        # generate true diff
        subprocess.run(['git', 'checkout', 'new_branch'],
                       check=False,
                       capture_output=False)
        gitdiff = subprocess.run(
            ['git', 'diff', 'origin/master', 'origin/new_branch'],
            capture_output=True)
        TRUE_DIFF = gitdiff.stdout.decode('utf-8')
        # generate coverage.xml
        subprocess.run([
            sys.executable, '-m', 'pytest', '--cov=src', '--cov-report', 'xml',
            'test'
        ])
        # # generate pylint_output.txt
        with open(PYLINT_OUTPUT_PATH, "w") as f:
            subprocess.run([sys.executable, '-m', 'pylint', 'src'], stdout=f)
        os.chdir(first_pwd)
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        os.remove(PYLINT_OUTPUT_PATH)
        os.remove(COVERAGE_XML_PATH)
        os.remove(REPO_PATH / '.coverage')
        return super().tearDownClass()

    def test_get_diff(self):
        diff = get_diff(REPO_PATH, "origin/master", "origin/new_branch").read()
        self.assertEqual(TRUE_DIFF, diff + "\n")

    def test_process_diff(self):
        self.assertDictEqual(process_diff(TRUE_DIFF), true_processed_diff)

    def test_process_coverage_xml(self):
        processed_cov = process_coverage_xml(COVERAGE_XML_PATH,
                                             repo_path=REPO_PATH)
        self.assertDictEqual(processed_cov, true_processed_cov)

    def test_filter_coverage(self):
        filtered_cov = filter_coverage(diff_dict=true_processed_diff,
                                       coverage_dict=true_processed_cov)
        self.assertDictEqual(filtered_cov, true_filtered_cov)

    def test_produce_ranges(self):
        lst = [1, 2, 3, 5, 6, 8, 10]
        ranges = [(1, 3), (5, 6), (8, 8), (10, 10)]
        self.assertEqual(produce_ranges(lst), ranges)

    def test_diff_coverage_report(self):
        report = diff_coverage_report(true_filtered_cov, show_missing=False)
        self.assertStrAlmostEqual(report, true_report)
        report = diff_coverage_report(true_filtered_cov, show_missing=True)
        self.assertStrAlmostEqual(report, true_report_missing)

    def test_process_lint_report(self):
        processed_lint = process_pylint_report(PYLINT_OUTPUT_PATH)
        self.assertDictEqual(processed_lint, true_processed_lint)

    def test_filter_lint(self):
        filtered_lint = filter_lint(true_processed_diff, true_processed_lint)
        self.assertDictEqual(filtered_lint, true_filtered_lint)

    def test_filter_lint_report(self):
        filtered_lint_report = diff_lint_report(true_filtered_lint)
        self.assertStrAlmostEqual(filtered_lint_report, true_lint_report)

    def test_full_report(self):
        diff_cov_lint(target_ref="master",
                      source_ref="new_branch",
                      cov_report=COVERAGE_XML_PATH,
                      lint_report=PYLINT_OUTPUT_PATH,
                      repo_path=REPO_PATH)
        captured = self.capsys.readouterr()
        self.assertStrAlmostEqual(captured.out, true_full_report)
